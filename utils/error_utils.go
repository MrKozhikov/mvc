package utils

type ApplicationError struct {
	Message string
	Status  int
	Code    string
}
