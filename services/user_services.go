package services

import (
	"gitlab.com/__Rakhmettolla/mvc/domain"
	"gitlab.com/__Rakhmettolla/mvc/utils"
)

func GetUser(userId int64) (*domain.User, *utils.ApplicationError) {
	return domain.GetUser(userId)
}
