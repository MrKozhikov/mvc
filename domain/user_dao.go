package domain

import (
	"gitlab.com/__Rakhmettolla/mvc/utils"
	"net/http"
)

var users = map[int64]*User{
	13: {
		Id:        13,
		FirstName: "Rakhmettolla",
		LastName:  "Kozhikov",
		Email:     "raha77811@gmail.com",
	},
	12: {
		Id:        12,
		FirstName: "Stephen",
		LastName:  "Carry",
		Email:     "nba@gmail.com",
	},
}

func GetUser(userId int64) (*User, *utils.ApplicationError) {
	if user := users[userId]; user != nil {
		return user, nil
	}
	return nil, &utils.ApplicationError{
		Message: "user not found",
		Status:  http.StatusNotFound,
		Code:    "not found",
	}
}
